﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SocketIterative
{
    class TCPEchoServer
    {
        public static void Main(string[] args)
        {
            IPAddress ip = IPAddress.Parse("192.168.1.2");

            TcpListener serverSocket = new TcpListener(ip, 6789);
            //Alternatively but deprecated
            //TcpListener serverSocket = new TcpListener(6789);


            serverSocket.Start();
            Console.WriteLine("Server started");
            while (true)
            {
                TcpClient connectionSocket = serverSocket.AcceptTcpClient();
                //Socket connectionSocket = serverSocket.AcceptSocket();
                Console.WriteLine("Server activated");
                EchoService echoService = new EchoService(connectionSocket);
                echoService.doIt();
            }
            //connectionSocket.Close();
            serverSocket.Stop();

        }
    }
}
